# README #

Este repositorio es con el fin de tener una Base de Wordpress

Tiene todo los plugins instalados, tiene el divi y además tiene el childtheme. Solo se tiene que cambiar la base de datos y el usuario de la base de datos en el config.
Además se tiene que cambiar la foto del chiltheme. 

* Versión de Wordpress
  5.3.2

* Plugins instalados 
- ManageWp
- All-in-One Wp Migratiion ( Usar el mismo de Colmena) o Duplicator 
- Classic Editor 
- Divi Accessibility
- Max upload size ( si llega a ser necesario ) 
- SVG Support
- Jetpack ( Depende) 
- Contact Form 7 ( en caso de requerirse) 
- Yoast SEO
- Wordfence Security
- Header and Footer Scripts o Header Footer Code Manager ( Este es para colocar el pixel de Facebook y la localización de google )
- Autoptimize
- WP Mail SMTP (Para que los mails no lleguen a spam) 
- Defender (Seguridad)

* Theme 
  Divi 4.0
  
  Este repositorio lo hizo Jaime Fossi 
  de IPH Digital 
  
